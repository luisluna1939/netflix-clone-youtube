import React, { useEffect } from 'react';
import './App.css';
import HomeScreen from './screens/HomeScreen.js';
import LoginScreen from './screens/LoginScreen.js';
import ProfileScreen from './screens/ProfileScreen';
import { auth } from './firebase';
import { useDispatch, useSelector } from 'react-redux';
import { login, logout, selectUser } from './features/userSlice';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";


function App() {

  //This brings the user from the User Store
  const user = useSelector(selectUser)
  //To dispatch the actions from the User (Redux store) we use this
  const dispatch = useDispatch()

  useEffect(() => {

    /** Firebase can listen and see the changes and knows if you're logged in, and
     * stores in the local memory a cookie that information
     */

    //Listener that listens any authentication state changes
    const unsubscribe = auth.onAuthStateChanged(userAuth => {
      if (userAuth) {
        //Logged in
        console.log(userAuth)
        /**The login action uses something called payload */
        dispatch(
          login({
            uid: userAuth.uid,
            email: userAuth.email,
          })
        )
      } else {
        //Logged out
        dispatch(logout())
      }
    })
    /** Unsubscribe is a tool of firebase that allows to clean the listener. we can return
     * this value. Instead of duplicate the listener it detach and attach to the same listener.
     * 
     * return unsubscribe is equivalent to:
     * 
     * return () => {
     *  unsubscribe()
     *  }
      */
    return unsubscribe
  }, [dispatch]) //UseEffect is dependent on dispatch

  return (
    <div className="app">
      {/* This router checks where we are on and acording to the Routes it renders 
      whatever is inside the route, for example the path "/" it renders the HomeScreen component */}

      <Router>
        {!user ? (
          <LoginScreen />
        ) : (
          <Switch>
            <Route exact path="/" > {/* exact path="" defines that it needs to render what's on that extact path */}
              <HomeScreen />
            </Route>
            <Route path="/profile">
              <ProfileScreen />
            </Route>
          </Switch>
        )}

      </Router>

    </div>
  );
}

export default App;
