import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import './Nav.css'

function Nav() {
    const [show, handleShow] = useState(false)
    //UseHistory() hook gives us the history
    const history = useHistory()

    const transitionNavBar = () => {
        if (window.scrollY > 100) {
            handleShow(true)
        } else {
            handleShow(false)
        }
    }

    useEffect(() => {
        window.addEventListener("scroll", transitionNavBar)
        return () => window.removeEventListener("scroll", transitionNavBar)
    }, [])

    return (
        <div className={`nav ${show && "nav__black"}`}>
            <div className="nav__contents">
                <img
                    //When click on the logo it leads us to the main path
                    onClick={() => history.push("/")}
                    className="nav__logo"
                    src="https://assets.stickpng.com/images/580b57fcd9996e24bc43c529.png"
                    alt="netflix-logo"
                />

                <img
                    //When click on the avatar it leads us to the profile path
                    onClick={() => history.push("/profile")}
                    className="nav__avatar"
                    src="https://upload.wikimedia.org/wikipedia/commons/0/0b/Netflix-avatar.png"
                    alt="netflix-avatar"
                />
            </div>
        </div>
    )
}

export default Nav
