import React, { useState, useEffect } from 'react'
import axios from './axios.js'
import Youtube from 'react-youtube'
import movieTrailer from 'movie-trailer'
import './Row.css'

function Row({ title, fetchUrl, isLargeRow = false }) {

    const [movies, setMovies] = useState([])

    //state to set a trailerURL
    const [trailerUrl, setTrailerUrl] = useState("")


    const base_url = "https://image.tmdb.org/t/p/original/"

    useEffect(() => {
        async function fetchData() {
            const request = await axios.get(fetchUrl)
            setMovies(request.data.results)
            return request
        }

        fetchData()
    }, [fetchUrl])

    /* Options for embedded youtube player. Took from youtube documentacion */
    const opts = {
        height: "390",
        width: "100%",
        playerVars: {
            //https://developers.google.com/youtube/player_parameters?hl=es
            autoplay: 1
        }
    }

    /* When we click a poster a trailer from youtube appears */
    const handleClick = (movie) => {
        /* First we set on empty the trailerUrl so player video does not appear on page load  */
        if (trailerUrl) {
            setTrailerUrl('')
        } else {
            /* movieTrailer(movieName) is a function from movie-trailer package.
            This searchs for a trailer URL on youtube */
            movieTrailer(movie?.title || movie?.name || movie?.original_name)
                .then(url => {
                    /*https://www.youtube.com/watch?v=XtMThy8QKqU&t=9209s
                    Being this "v=XtMThy8QKqU&t=9209s" the videoId 
                    
                    To get that, we use this next line
                    */
                    const urlParams = new URLSearchParams(new URL(url).search)
                    //We get the id that's after v=
                    setTrailerUrl(urlParams.get('v'))
                }).catch(error => console.log(error))
        }
    }

    return (
        <div className="row">
            <h2>{title}</h2>

            <div className="row__posters">
                {movies.map((movie) => {
                    /* ((isLargeRow && movie.poster_path) ||
                        (!isLargeRow && movie.backdrop_path)) 
                        Allows to avoid to render broken links
                        */
                    return ((isLargeRow && movie.poster_path) ||
                        (!isLargeRow && movie.backdrop_path)) && (
                            <img
                                className={`row__poster ${isLargeRow && "row__posterLarge"}`}
                                key={movie.id}
                                onClick={() => handleClick(movie)}
                                src={`${base_url}${isLargeRow ? movie?.poster_path : movie?.backdrop_path}`} alt={movie?.name} />
                        )
                })}
            </div>
            {/*If trailerUrl is there then we show the player */}
            {trailerUrl && <Youtube videoId={trailerUrl} opts={opts} />}
        </div>
    )
}

export default Row
