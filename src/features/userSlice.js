import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  user: null
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {

    /**This are called actions and we can dispatch them and they execute the behaviour we define 
     * 
     * 
    */
    login: (state, action) => {
      /** State modifies the user and uses whateaver we pass it to on the payload */
      state.user = action.payload
    },
    logout: (state) => {
      state.user = null
    }
  },
});

//This gives us access to the actions defined
export const { login, logout } = userSlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state: RootState) => state.counter.value)`
export const selectUser = (state) => state.user.user;

export default userSlice.reducer;
