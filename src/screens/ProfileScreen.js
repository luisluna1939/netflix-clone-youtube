import React from 'react'
import { selectUser } from '../features/userSlice'
import { useSelector } from 'react-redux'
import { auth } from '../firebase'
import Nav from '../Nav'
import './ProfileScreen.css'

function ProfileScreen() {
    const user = useSelector(selectUser)

    return (
        <div className="profileScreen">
            <Nav />
            <div className="profileScreen__body">
                <h1>Edit Profile</h1>
                <div className="profileScreen__info">
                    <img
                        src="https://upload.wikimedia.org/wikipedia/commons/0/0b/Netflix-avatar.png"
                        alt="netflix-avatar"
                    />
                    <div className="profileScreen__details">
                        <h2>{user.email}</h2>
                        <div className="profileScreen__plans">
                            <h3>Plans</h3>
                            <div className="profileScreen__date">Renewal date: 04/12/2022</div>

                            <div className="profileScreen__option">
                                <div className="profileScreen__quality">
                                    <div>Netflix Standard</div>
                                    <div>1080p</div>
                                </div>
                                <button className="profileScreen__subscribe">Subscribe</button>
                            </div>

                            <div className="profileScreen__option">
                                <div className="profileScreen__quality">
                                    <div>Netflix Basic</div>
                                    <div>480p</div>
                                </div>
                                <button className="profileScreen__subscribe">Subscribe</button>
                            </div>

                            <div className="profileScreen__option">
                                <div className="profileScreen__quality">
                                    <div>Netflix Premium</div>
                                    <div>4K+HDR</div>
                                </div>
                                <button className="profileScreen__subscribe">Subscribe</button>
                            </div>
                            
                            <button
                                //With the auth object from firebase we can logout easily with auth.signOut()
                                onClick={() => auth.signOut()}
                                className="profileScreen__signOut"
                            >
                                Sign Out
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProfileScreen
